;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'sgc
  authors "Dima Akater"
  first-publication-year-as-string "2021"
  org-files-in-order '("sgc")
  site-lisp-config-prefix "50"
  license "GPL-3")
