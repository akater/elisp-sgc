;;;  -*- lexical-binding: t -*-

;; Version: 20230609.042251

(load "fakemake-buildenv" nil t)
(load "fakemake-done" nil t)

;; Don't litter.
(setq backup-by-copying t
      backup-directory-alist `(("." . ,(expand-file-name
                                        ".saves" user-emacs-directory)))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control nil)

(defun fakemake-message (format-string &rest args)
  (apply #'message (concat "%s: " format-string)
         'fakemake args))

(defvar fakemake-ignore-null-license nil)

(defvar fakemake-org-files-for-testing-in-order)
;; (defvar fakemake-source-files-for-testing-in-order)

(fakemake-done-defvar fakemake-elisp-source-files-in-order nil)

(fakemake-done-defvar fakemake-files-to-install-into-lispdir nil)

(fakemake-done-defvar fakemake-special-filenames
  (list (rx string-start "Makefile")
        (rx string-start "README")
        (rx string-start "site-gentoo.d" string-end)
        (rx string-start ".git" string-end)
        (rx string-start ".gitignore" string-end)
        (rx string-start "lisp" string-end)
        (rx string-start "contrib" string-end)
        (rx string-start "fakemake" string-end)
        (rx string-start "etc" string-end)))

(defvar fakemake-files-to-load-for-testing-in-order nil)

(defun fakemake--file-from-elisp-source-directory-p (file)
  (cl-member (file-name-directory
              (expand-file-name file fakemake-project-root))
             fakemake-elisp-source-directories
             :test #'string-equal :key #'expand-file-name))

(defconst org-sources-directory (or org-local-sources-directory
                                    org-upstream-sources-directory)
  "Directory with Org source files to link Elisp source files to.

Meant to be set by fakemake.")

(when org-sources-directory
  (unless (stringp org-sources-directory)
    (error "Error: %s is not a string: %s"
           'org-sources-directory org-sources-directory))
  (unless (string-prefix-p "/" org-sources-directory)
    (error "Error: Org sources directory is ambiguous: %s"
           org-sources-directory))
  (fakemake-message "%s is set to %s"
                    'org-sources-directory org-sources-directory)
  ;; TODO: This message should not be seen after prepare;
  ;; probably the whole form should not be evaluated then.
  (fakemake-message "Links in el source will point to it"))

;; this is likely only needed when org had been ripped off Emacs
;; and installed separately
(require 'org)

(require 'org-element)
(setq org-element-cache-persistent nil)

;; silence some messages
(defalias 'org-development-elisp-mode 'org-mode)

(defun fakemake-clean ()
  (do-files-recursively (file fakemake-project-root
                              :do-not-descend '(".git/"))
    (unless (cl-member file fakemake-original-files :test #'string-equal)
      (if (file-directory-p file)
          (delete-directory file t)
        (delete-file file)))))

(defun fakemake-process-special-dirs ()
  (when (file-directory-p "contrib")
    (cl-pushnew "contrib" fakemake-elisp-source-directories
                :test #'string-equal))
  (when (file-directory-p "lisp")
    (setq fakemake-elisp-source-directories
          (cl-delete fakemake-project-root
                     fakemake-elisp-source-directories
                     :test #'string-equal))
    (cl-pushnew "lisp" fakemake-elisp-source-directories
                :test #'string-equal)))

(unless (fakemake-donep 'compile)
  (when (memq 'test use-flags)
    (let (testing-thunk-is-found)
      (cond
       ((boundp 'fakemake-org-files-for-testing-in-order)
        (if (listp fakemake-org-files-for-testing-in-order)
            (setq fakemake-org-files-in-order (append
                                               fakemake-org-files-in-order
                                               fakemake-org-files-for-testing-in-order)
                  testing-thunk-is-found t)
          (error "%s must be a list but it's value is %s"
                 'fakemake-org-files-for-testing-in-order
                 fakemake-org-files-for-testing-in-order)))
       (t
        (fakemake-message "Guessing how testing %s is arranged in it..."
                          fakemake-feature)
        (if-let ((tests-org-source (file-exists-p+
                                    (format "%s-tests.org" fakemake-feature))))
            (progn
              (setq fakemake-org-files-for-testing-in-order
                    (list tests-org-source))
              (if (cl-member tests-org-source fakemake-org-files-in-order
                             :test #'string-equal)
                  (fakemake-message "Found org source file %s already mentioned in the system definition"
                                    tests-org-source)
                (fakemake-message "Registering org source file %s" tests-org-source)
                (setq fakemake-org-files-in-order
                      (append fakemake-org-files-in-order
                              fakemake-org-files-for-testing-in-order)
                      testing-thunk-is-found t)))
          (setq fakemake-org-files-for-testing-in-order nil)
          (fakemake-message "No bundled testing found"))))
      (when (and testing-thunk-is-found
                 fakemake-org-files-for-testing-in-order)
        ;; What follows is a hack.
        ;; TODO: set up output-files functions or variables instead,
        ;; record them in directory or file .fakemake-state
        ;; or some place like this
        (setq fakemake-files-to-load-for-testing-in-order
              (mapcar (lambda (x) (concat (file-name-base x) ".el"))
                      fakemake-org-files-for-testing-in-order))
        (fakemake-message "Blindly assuming testing code will be in %s"
                          (mapconcat #'identity
                                     fakemake-files-to-load-for-testing-in-order
                                     ", "))))
    (fakemake-done-defvar fakemake-org-files-for-testing-in-order)
    (fakemake-done-defvar fakemake-files-to-load-for-testing-in-order)
    ;; (fakemake-done-defvar fakemake-source-files-for-testing-in-order)
    ))

(defun insert-gpl-3-license
    (one-line-to-give-the-programs-name-and-an-idea-of-what-it-does
     initial-year-as-string authors &optional buffer)
  (setq buffer (or buffer (current-buffer)))
  (with-current-buffer (find-file-noselect
                        (expand-file-name "COPYING" fakemake-project-root))
    (goto-char (point-min))
    (save-match-data
      (re-search-forward
       (rx line-start "How to Apply These Terms to Your New Programs"))
      (re-search-forward
       (rx line-start
           "<one line to give the program's name and a brief idea of what it does.>"))
      (let ((start (with-current-buffer buffer (point))))
        (with-current-buffer buffer
          (insert
           one-line-to-give-the-programs-name-and-an-idea-of-what-it-does ?\n))
        (re-search-forward (rx line-start))
        (let ((copyright-pattern
               (if (not (looking-at-p "Copyright"))
                   (error "Can't find copyright line in the template")
                 (buffer-substring-no-properties
                  (point) (progn (end-of-line) (point)))))
              (years (let ((current-year (format-time-string "%Y")))
                       (if (string-equal initial-year-as-string current-year)
                           current-year
                         (concat initial-year-as-string "--" current-year)))))
          (with-current-buffer buffer
            (insert
             (fold (lambda (string rule)
                     (replace-regexp-in-string (car rule) (cdr rule) string))
                   copyright-pattern
                   `((,(rx "<year>") . ,years)
                     (,(rx "<name of author>") . ,authors))))))
        (let ((rest-of-the-license (buffer-substring-no-properties
                                    (point)
                                    (progn (re-search-forward
                                            (rx line-start "Also add"))
                                           (beginning-of-line) (point)))))
          (with-current-buffer buffer
            (insert rest-of-the-license)
            (comment-region start (point))))))
    (kill-buffer)))

(defun license-normalize (license)
  (when license
    (alist-get
     license
     ;; TODO: Generate variations
     '(
       ;; gpls
       (( "gpl-3"    "gpl-3+"    "GPL-3"    "GPL-3+")     .  "gpl-3.0")
       (( "gpl-2"    "gpl-2+"    "GPL-2"    "GPL-2+")     .  "gpl-2.0")
       (("agpl-3"   "agpl-3+"   "AGPL-3"   "AGPL-3+")     . "agpl-3.0")
       (("lgpl-3"   "lgpl-3+"   "LGPL-3"   "LGPL-3+")     . "lgpl-3.0")
       (("lgpl-2.1" "lgpl-2.1+" "LGPL-2.1" "LGPL-2.1+")   . "lgpl-2.1")
       (("gfdl-1.3" "gfdl-1.3+" "GFDL-1.3" "GFDL-1.3+")   . "gfdl-1.3")
       ;; and alike (still allows +)
       (("lppl-1.3" "lppl-1.3+" "LPPL-1.3" "LPPL-1.3+")   . "lppl-1.3c")
       ;; other versioned
       (("cc-by-sa-4" "CC-BY-SA-4")                       . "cc-by-sa-4.0")
       ;; sometimes-not-versioned
       ((     "epl-1" "epl"           "EPL-1")            . "epl-1.0")
       ((  "apache-2" "apache"     "Apache-2" "Apache")   . "apache-2.0")
       (("artistic-2" "artistic" "Artistic-2" "Artistic") . "artistic-2.0")
       ;; quasi-versioned
       (("bsd-2" "BSD-2")                                 . "bsd-2-clause")
       (("bsd-3" "BSD-3")                                 . "bsd-3-clause")
       ;; rest
       (("unlicense" "UNLICENSE" "Unlicense")             . "unlicense")
       (("mozilla"   "MOZILLA"   "Mozilla")               . "mozilla")
       (("mit"       "MIT")                               . "mit"))
     license nil
     (lambda (key l)
       (cl-member l key :test #'string-equal)))))

(defmacro license-normalizef (place)
  `(setf ,place (license-normalize ,place)))

(defun fakemake--license-file-exists-p (&optional directory)
  (setq directory (or directory fakemake-project-root))
  (cl-some (lambda (x) (file-exists-p+ (expand-file-name x directory)))
           '("COPYING" "LICENSE")))

(defun insert-elisp-preamble (tangled-file)
  (with-current-buffer (find-file-noselect tangled-file)
    (let ((org-file (file-exists-p+ (concat (file-name-base tangled-file)
                                            ".org"))))
      (let ((name (file-name-nondirectory tangled-file))
            (description
             (when org-file
               (with-current-buffer (find-file-noselect org-file)
                 (save-match-data
                   (when (re-search-forward
                          (rx line-start "#+description: ")
                          nil t)
                     (buffer-substring-no-properties
                      (point) (line-end-position)))))))
            (local-variables '(:lexical-binding t)))
        (let ((license (or (when org-file
                             (with-current-buffer (find-file-noselect org-file)
                               (save-match-data
                                 (when (re-search-forward
                                        (rx line-start "#+license: ")
                                        nil t)
                                   (buffer-substring-no-properties
                                    (point) (line-end-position))))))
                           (ignore-errors fakemake-license))))
          (license-normalizef license)
          (cond
           ((null license)
            (if fakemake-ignore-null-license
                (with-current-buffer (find-file-noselect tangled-file)
                  (goto-char (point-min))
                  (apply #'elisp-insert-header
                         :first-publication-year
                         fakemake-first-publication-year-as-string
                         :authors fakemake-authors
                         (plist-with-keys name description local-variables))
                  (comment-region (line-beginning-position)
                                  (line-end-position))
                  (save-buffer))
              (error "No license specified for %s" name)))
           ((aand (string-equal "gpl-3.0" license)
                  (fakemake--license-file-exists-p)
                  (with-current-buffer (find-file-noselect it)
                    (and (ignore-errors
                           (save-excursion
                             (goto-char (point-min))
                             (search-forward
                              "How to Apply These Terms to Your New Programs"
                              nil t))
                           t)
                         (ignore-errors
                           (save-excursion
                             (goto-char (point-min))
                             (save-match-data
                               (and (re-search-forward
                                     (rx line-start
                                         "GNU GENERAL PUBLIC LICENSE")
                                     nil t)
                                    (looking-at-p
                                     (rx (one-or-more whitespace)
                                         "Version 3" (not digit))))))))))
            ;; The obsessive case, left here mostly due to personal tastes
            (goto-char (point-min))
            (insert-gpl-3-license
             (with-temp-buffer
               (apply #'elisp-insert-header
                      (plist-with-keys name description local-variables))
               (buffer-string))
             fakemake-first-publication-year-as-string
             fakemake-authors)
            (save-buffer))
           ((fboundp 'lice)
            (let ((start (point)))
              (apply #'elisp-insert-header
                     :first-publication-year
                     fakemake-first-publication-year-as-string
                     :authors fakemake-authors
                     (plist-with-keys name description local-variables))
              (comment-region start (point)))
            (insert ?\n ?\n)
            (let ((lice:header-spec '(lice:insert-license))
                  (lice:program-name fakemake-feature-full-name))
              (lice license))
            (save-buffer))
           (t
            (let (license-file)
              (if (not (setq license-file (fakemake--license-file-exists-p)))
                  (error "Don't know how to deal with the license")
                (let ((start (point)))
                  (apply #'elisp-insert-header
                         :first-publication-year
                         fakemake-first-publication-year-as-string
                         :authors fakemake-authors
                         (plist-with-keys name description local-variables))
                  (comment-region start (point)))
                (insert ?\n ?\n)
                (let* ((start (point))
                       (end (+ start
                               (cadr
                                (insert-file-contents-literally
                                 license-file)))))
                  (comment-region start end))
                (save-buffer))))))))))

(defun insert-newline-to-please-unix () (insert ?\n))

(defun insert-elisp-postamble (file)
  (with-current-buffer (find-file-noselect file)
    (goto-char (point-max))
    (unless (cl-member (file-name-base file)
                       (ignore-errors fakemake-exceptions-to-provide)
                       ;; Sometimes buggy byte-compiled files are produced,
                       ;; and omitting `provide' kinda helps.
                       :test #'string-equal)
      (insert ?\n)
      (prin1 `(provide ',(intern (file-name-base file))) (current-buffer)))
    (insert ?\n ?\n
            (file-name-nondirectory file) " ends here")
    (comment-region (line-beginning-position) (line-end-position))
    (insert-newline-to-please-unix)
    (save-buffer) (kill-buffer))
  file)

(require 'ob-tangle)

(defvar ob-flags)

(require 'ol)
(defun fakemake--org-redirect-link (link directory)
  (if (and (string-match org-link-types-re link)
	   (string= (match-string 1 link) "file"))
      (concat "file:"
	      (expand-file-name
               ;; basically exactly how it's done in ob-tangle
               (file-relative-name (substring link (match-end 0)))
	       directory))
    link))

(defun fakemake--org-links-disabled-p (alist)
  (aand (assq :comments alist)
        (or (stringp+ (cdr it))
            (aif (functionp+ (cdr it))
                (or (stringp+ (funcall it))
                    (error "Element of unexpected type returned by the closure in Org parameter :comments"))
              (error "Element of unexpected type in Org parameter :comments")))
        (cl-find it '("no" "org") :test #'string-equal)))

(defmacro with-org-links-redirection-to (directory &rest body)
  (declare (indent 1))
  (let ((spec (gensym "spec-"))
        (directory-g (gensym "org-links-redirection-directory-")))
    `(let ((,directory-g ,directory))
       (cl-check-type ,directory-g (or null string))
       (if ,directory-g
           (cl-flet ((override-link-in-spec (,spec)
                       (setf (caddr ,spec)
                             (fakemake--org-redirect-link
                              (caddr ,spec)
                              ,directory-g))
                       ,spec))
             (advice-add 'org-babel-tangle-single-block
                         :filter-return #'override-link-in-spec)
             (unwind-protect (let ((org-babel-default-header-args
                                    org-babel-default-header-args))
                               (unless (fakemake--org-links-disabled-p
                                        org-babel-default-header-args)
                                 (push '(:comments . "link")
                                       ;; It is unclear
                                       ;; if we can actually push
                                       ;; a constant cons here.
                                       org-babel-default-header-args))
                               ,@body)
               (advice-remove 'org-babel-tangle-single-block
                              #'override-link-in-spec)))
         ,@body))))

(defmacro fakemake--do-org-lines (file &rest alist)
  "Like `org-babel-map-src-blocks' but linewise, checking other conditions.

ALIST is a list of cond clauses; the (src-block . BODY) clause is
treated specially but the check for src-block happens after all
other clauses."
  (declare (indent 1))
  (let ((next-line-code `((end-of-line)
                          (forward-char 1))))
    (let ((body (alist-get 'src-block alist))
          (clauses (cl-loop for clause
                            in (cl-remove 'src-block alist
                                          :key #'car :test #'eq)
                            collect (cons (car clause)
                                          (append (cdr clause)
                                                  next-line-code))))
          (tempvar (make-symbol "file")))
      `(let* ((case-fold-search t)
	      (,tempvar ,file)
	      (visited-p (or (null ,tempvar)
			     (get-file-buffer (expand-file-name ,tempvar))))
	      (point (point)) to-be-removed)
         (save-window-excursion
	   (when ,tempvar (find-file ,tempvar))
	   (setq to-be-removed (current-buffer))
	   (goto-char (point-min))
	   (until (eobp)
             (cond
              ,@clauses
              ((looking-at org-babel-src-block-regexp)
	       (when (org-babel-active-location-p)
	         (goto-char (match-beginning 0))
	         (let ((full-block (match-string 0))
		       (beg-block (match-beginning 0))
		       (end-block (match-end 0))
		       (lang (match-string 2))
		       (beg-lang (match-beginning 2))
		       (end-lang (match-end 2))
		       (switches (match-string 3))
		       (beg-switches (match-beginning 3))
		       (end-switches (match-end 3))
		       (header-args (match-string 4))
		       (beg-header-args (match-beginning 4))
		       (end-header-args (match-end 4))
		       (body (match-string 5))
		       (beg-body (match-beginning 5))
		       (end-body (match-end 5)))
                   ;; Silence byte-compiler in case `body' doesn't use all
                   ;; those variables.
                   (ignore full-block beg-block end-block lang
                           beg-lang end-lang switches beg-switches
                           end-switches header-args beg-header-args
                           end-header-args body beg-body end-body)
                   ,@body
	           (goto-char end-block) ,@next-line-code)))
              (t ,@next-line-code))))
         (unless visited-p (kill-buffer to-be-removed))
         (goto-char point)))))

(defmacro fakemake--map-src-blocks (file &rest body)
  (declare (indent 1))
  `(fakemake--do-org-lines ,file
     ((looking-at (rx (one-or-more ?*) whitespace
                      (group (one-or-more not-newline))))
      (message "Processing section %s" (match-string-no-properties 1)))
     ;; ((there is link on the line)
     ;;  blah blah)
     (src-block ,@body)))

(eval-and-compile
  (defvar fakemake--collect-blocks-function nil)
  (defmacro with-surgery-on-collect-blocks-function (&rest body)
    "Evaluate BODY with a redefined #'org-babel-tangle-collect-blocks.

Within BODY, `org-babel-tangle-collect-blocks' is redefined dynamically
to perform additional tasks in a single pass of traversing an Org file."
    (let ((original-function (make-symbol "original-function"))
          (org-collect-blocks (make-symbol "org-collect-blocks")))
      `(let ((,org-collect-blocks
              (let ((definition
                      (find-definition-noselect
                       'org-babel-tangle-collect-blocks nil)))
                (with-current-buffer (car definition)
                  (prog1 (save-excursion
                           (goto-char (cdr definition))
                           (read (current-buffer)))
                    (kill-buffer))))))
         (cl-letf (((symbol-function 'org-babel-tangle-collect-blocks)
                    (with-memoization fakemake--collect-blocks-function
                      (cl-letf (((symbol-function 'org-babel-map-src-blocks)
                                 (symbol-function 'fakemake--map-src-blocks)))
                        (let ((lexical-binding t))
                          (byte-compile (eval ,org-collect-blocks t))))))) 
           ,@body)))))

(defmacro do-file-names-as-in-org-babel-tangle (&rest body)
  (declare (indent 0))
  ;; There is no interface to individual tangle operations
  ;; so we just copy the code the way it is in Emacs now.
  ;; For different versions of Emacs, different code should be used here.
  ;;
  ;; Instead of this,
  ;; Org should simply allow to append to a file when tangling
  ;; or, if that's forbidden to it by its gods,
  ;; have (lambda (by-fn) (let ((file-name (car by-fn))) ..) ..)
  ;; that's called on individual file names, as a named function
  ;; so that we can advise it rather than org-babel-tangle,
  ;; as we do below.

  ;; Still better is for org-babel-tangle to be a generic function
  (let ((by-fn (make-symbol "by-fn")))
    `(save-restriction
       (save-excursion
         (let ((org-babel-default-header-args
	        (if target-file
		    (org-babel-merge-params org-babel-default-header-args
					    (list (cons :tangle target-file)))
	          org-babel-default-header-args))
	       (tangle-file
	        (when (equal arg '(16))
	          (or (cdr (assq :tangle (nth 2 (org-babel-get-src-block-info 'light))))
		      (user-error "Point is not in a source code block")))))
           (dolist ( ,by-fn
                     (if (equal arg '(4))
	                 (org-babel-tangle-single-block 1 t)
	               (org-babel-tangle-collect-blocks lang-re tangle-file)))
             (let ((file-name (car ,by-fn))) (when file-name ,@body))))))))

(defmacro with-org-babel-tangle-appending (&rest body)
  ;; Note: (run-hooks 'org-babel-pre-tangle-hook)
  ;; is only evaluated after the contents of the files are preserved.
  (declare (indent 0))
  (let ((preserved-alist (make-symbol "preserved-alist")))
    `(let (,preserved-alist)
       (cl-flet ((preserve (&optional arg target-file lang-re)
                   (ignore lang-re)
                   ;; target-file and lang-re are used implicitly
                   ;; in do-file-names-as-in-org-babel-tangle
                   (do-file-names-as-in-org-babel-tangle
                     (with-current-buffer
                         (or (alist-get file-name
                                        ,preserved-alist
                                        nil nil
                                        #'string-equal)
                             (let ((temp-buffer (generate-new-buffer
                                                 " *temp*")))
                               (push (cons file-name temp-buffer)
                                     ,preserved-alist)
                               temp-buffer))
                       (when (file-exists-p file-name)
                         (insert-file-contents-literally file-name)))))
                 (restore (&optional arg target-file lang-re)
                   (ignore lang-re)
                   ;; target-file and lang-re are used implicitly
                   ;; in do-file-names-as-in-org-babel-tangle
                   (do-file-names-as-in-org-babel-tangle
                     (when (file-exists-p file-name)
                       (with-current-buffer (find-file-noselect file-name)
                         (save-excursion
                           (goto-char (point-min))
                           (insert-buffer-substring
                            (or (alist-get file-name
                                           ,preserved-alist
                                           nil nil
                                           #'string-equal)
                                (error "fakemake: New file name to tangle to appeared while tangling"))))
                         (save-buffer) (kill-buffer))))))
         (advice-add 'org-babel-tangle :before #'preserve)
         (advice-add 'org-babel-tangle :after #'restore)
         (unwind-protect (cl-locally ,@body)
           (advice-remove 'org-babel-tangle #'restore)
           (advice-remove 'org-babel-tangle #'preserve)
           (mapc (lambda (x) (kill-buffer (cdr x))) ,preserved-alist))))))

(defmacro with-org-babel-set-up (&rest body)
  (declare (indent 0))
  `(let (org-babel-tangle-use-relative-file-links
         (ob-flags use-flags))
     (with-org-links-redirection-to org-sources-directory
       (with-org-babel-tangle-appending ,@body))))

(defun fakemake--string-member-p (string list)
  (cl-member string list :test #'string-equal))

(define-error 'fakemake-tangling-error "Tangling error")
(defun fakemake-tangling-error (&rest args)
  (apply #'signal 'fakemake-tangling-error args))

(define-error 'fakemake-tangling-block-inconsistency-error
  "src block inconsistency first met in %s at line %s"
  'fakemake-tangling-error)
(defun fakemake-tangling-block-inconsistency-error (org-buffer start-line)
  (signal 'fakemake-tangling-block-inconsistency-error
          (list org-buffer start-line)))

(define-error 'fakemake-tangling-elisp-block-inconsistency-error
  "%s is different in blocks constituting elisp file %s; inconsistency first met in %s at line %s"
  'fakemake-tangling-block-inconsistency-error)

(defun fakemake-tangling-elisp-block-inconsistency-error ( parameter
                                                           elisp-file
                                                           org-buffer
                                                           start-line)
  (signal 'fakemake-tangling-elisp-block-inconsistency-error
          (list parameter elisp-file org-buffer start-line)))

(defconst fakemake-org-languages-alist
  '(("common-lisp" "lisp")
    ("emacs-lisp" "elisp"))
  "List of languages, with elements (CANONICAL-LANGUAGE-NAME . SYNONYMS).

The language name and all synonyms are strings.")

(defmacro awhen1 (test &rest body)
  (declare (indent 1))
  `(let ((it ,test))
     (when1 it ,@body)))

(defun fakemake--merge-tangled-file-entity-1 (tangled-file-entity
                                              cons-lang-block
                                              &optional buffer)
  (setq buffer (or buffer (current-buffer)))
  (cl-destructuring-bind ( existing-language
                           existing-start-line
                           existing-file
                           existing-link
                           existing-source-name
                           existing-params
                           existing-body
                           existing-comment) (cdr tangled-file-entity)
    ;; The spec below (and above but it's temporarily)
    ;; comes from the docstring of `org-babel-spec-to-string'

    ;; To quote the specification of org-babel-tangle-collect-blocks
    ;; on the matter,
    ;; > Return an association list of language and source-code block
    ;; > specifications of the form used by ‘org-babel-spec-to-string’
    ;; > grouped by tangled file name.

    ;; we are mostly interested in
    ;; - license
    ;; - org files tangled from
    ;; - org files referencing content in this file (by #+name links, usually)
    ;;   (is this called “backlinks”?)
    ;; - subtitle, description
    (cl-destructuring-bind ( incoming-language
                             incoming-start-line
                             incoming-file
                             incoming-link
                             incoming-source-name
                             incoming-params
                             incoming-body
                             incoming-comment) cons-lang-block
      (when incoming-source-name
        (message "Named block %s" incoming-source-name))
      (let (language-normalized)
        (cl-flet ((normalize-language (&optional (language incoming-language))
                    "If equivalence class is known, return it and set the canonical name."
                    (awhen1 (cl-find language
                                     fakemake-org-languages-alist
                                     :test #'fakemake--string-member-p)
                      (setq language-normalized (car it)))))
          (message "Comparing languages: %s vs %s"
                   existing-language incoming-language)
          (unless (or (string-equal existing-language incoming-language)
                      ;; Most often, they are simply equal.
                      ;; Otherwise, we check if they are from the same
                      ;; equivalence class:
                      (fakemake--string-member-p existing-language
                                                 (normalize-language)))
            (message "Different languages: %s, %s"
                     existing-language incoming-language)
            (fakemake-tangling-elisp-block-inconsistency-error
             ;; ; 'major-mode
             "Language (major mode)" (car tangled-file-entity)
             buffer incoming-start-line))
          (unless language-normalized (normalize-language)))
        ;; language-specific stuff
        (when (aand language-normalized (string-equal "emacs-lisp" it))
          ;; elisp-specific stuff

          (let ((existing-lexical-binding (assq :lexical existing-params))
                (incoming-lexical-binding (assq :lexical incoming-params)))
            (message "%s: %s vs %s" 'lexical-binding
                     existing-lexical-binding incoming-lexical-binding)
            (unless (and existing-lexical-binding
                         incoming-lexical-binding
                         (string-equal (cdr existing-lexical-binding)
                                       (cdr incoming-lexical-binding)))
              (fakemake-tangling-elisp-block-inconsistency-error
               'lexical-binding (car tangled-file-entity)
               buffer incoming-start-line)))

          (let ((existing-no-byte-compile (assq :no-byte-compile
                                                existing-params))
                (incoming-no-byte-compile (assq :no-byte-compile
                                                incoming-params)))
            (message "%s: %s vs %s" 'no-byte-compile
                     existing-no-byte-compile incoming-no-byte-compile)
            (unless (and existing-no-byte-compile
                         incoming-no-byte-compile
                         (string-equal (cdr existing-no-byte-compile)
                                       (cdr incoming-no-byte-compile)))
              (fakemake-tangling-elisp-block-inconsistency-error
               'no-byte-compile (car tangled-file-entity)
               buffer incoming-start-line)))))))
  tangled-file-entity)

(defmacro fakemake-tangle--merge-metadata (index tangled-file)
  "Merge metadata about TANGLED-FILE into place INDEX; don't cons too much."
  (let ((tangled-file-g (make-symbol "tangled-file-entity"))
        (entry (make-symbol "entry")))
    `(let ((,tangled-file-g
            ;; todo: (normalize-file-name ,tangled-file)
            ,tangled-file))
       ,@(cl-psetf tangled-file tangled-file-g)
       (unless (consp ,tangled-file)
         (error "Unknown container for tangled-file: %s" ,tangled-file))
       (if-let ((,entry (cl-assoc (car ,tangled-file)
                                  ,index :test #'string-equal)))
           (fold #'fakemake--merge-tangled-file-entity-1
                 ,entry (cdr ,tangled-file))
         ;; Instead of doing this shit,
         ;; I should have had access to internals of tangle-collect-blocks
         (push (prog1 (cons (car ,tangled-file)
                            ;; todo: we only need some components of the tree
                            (copy-tree (cadr ,tangled-file)))
                 (message
                  "Recording entry for a new file %s with language %s tangled from file %s"
                  (car ,tangled-file)
                  (caar (cdr ,tangled-file))
                  (nth 2 (car (cdr ,tangled-file)))))
               ,index)
         (fold #'fakemake--merge-tangled-file-entity-1
               (car ,index) (cddr ,tangled-file))))))

(defmacro with-org-src-blocks-metadata (var &rest body)
  (declare (indent 1))
  `(let (,(or var (setq var (make-symbol "var"))))
     ;; index is a value of type similar to (derivable from)
     ;; the one that org-babel-tangle-collect-blocks returns
     ;; and merge-metadata combines them recording new ones into index
     (cl-flet (,(let ((alist (make-symbol "alist"))
                      (tangled-file (make-symbol "tangled-file")))
                  `( combine-metadata(,alist)
                     (dolist (,tangled-file ,alist ,alist)
                       (fakemake-tangle--merge-metadata ,var ,tangled-file)))))
       (advice-add 'org-babel-tangle-collect-blocks :filter-return
                   #'combine-metadata)
       (unwind-protect (cl-locally ,@body)
         (advice-remove 'org-babel-tangle-collect-blocks
                        #'combine-metadata)))))

(defvar fakemake-decorate-regexps-blacklist nil)
(defun fakemake-decorate (tangled-file)
  "Post-process elisp-source file TANGLED-FILE."
  (unless (cl-member (file-relative-name tangled-file fakemake-project-root)
                     fakemake-decorate-regexps-blacklist
                     :test #'string-matched-p)
    (insert-elisp-preamble tangled-file)
    (insert-elisp-postamble tangled-file)))

(defun fakemake-prepare ()
  (let* (tangle-index
         (all-tangled-source-files
          (let (all-tangled-source-files)
            (dolist (file fakemake-org-files-in-order
                          (nreverse all-tangled-source-files))
              (dolist (tangled (let ((org-file (concat file ".org")))
                                 (if (file-exists-p org-file)
                                     ;; (with-org-tangle-session tangle-index
                                     ;;  (org-babel-tangle-file org-file))
                                     (with-org-babel-set-up
                                      (org-babel-tangle-file org-file))
                                   (fakemake-message "Skipping non-existing org file %s"
                                                     org-file)
                                   nil)))
                (setq tangled (file-relative-name tangled
                                                  fakemake-project-root))
                ;; TODO: if the file in question is in org-files-for-testing
                ;; record its outputs as source-files-for-testing
                (when (and
                       (not (string-match-p (rx string-start "etc/") tangled))
                       (string-match-p (rx ".el" string-end) tangled))
                  ;; TODO: We probably better record files of other types too
                  (cl-pushnew tangled all-tangled-source-files
                              :test #'string-equal)))))))
    (setq fakemake-elisp-source-files-in-order
          ;; these won't really be “in order”
          ;; TODO: Do something about this

          ;; We want a copy, always
          ;; but cl-remove will only do one if necessary
          ;; so we do it manually:
          (let ((tail all-tangled-source-files) copy)
            (while tail
              (let ((elt (pop tail)))
                (unless (or (string-match-p (rx string-start "lisp/") elt)
                            (fakemake--file-from-elisp-source-directory-p
                             elt))
                  (push elt copy))))
            (nreverse copy)))
    (dolist (tangled-file all-tangled-source-files)
      (unless (aand (file-name-directory tangled-file)
                    (string-equal "site-gentoo.d" it))
        (fakemake-decorate tangled-file))))
  ;; For more consistent intermediate buildenvs,
  (fakemake-process-special-dirs)
  'prepared)

(defun fakemake-config ()
  ;; when site-lisp-configuration is absent,
  ;; any config beyond in-source autoloads is discarded
  (when autoloads-file
    ;; I generally do not `require' in defuns
    ;; but this file is one-time init
    (require 'autoload)
    (let ((generated-autoload-file (expand-file-name
                                    (if site-autoloads
                                        (format "site-gentoo.d/%s-gentoo.el"
                                                fakemake-feature)
                                      autoloads-file))))
      (with-current-buffer (find-file-noselect generated-autoload-file)
        (ensure-directory default-directory)
        (save-buffer)                   ; file should exist
        (if (not site-autoloads) (progn (kill-buffer)
                                        (update-directory-autoloads
                                         default-directory))
          (let ((inhibit-read-only t))
            (goto-char (point-min))
            (insert ?\n
                    (format ";;; %s site-lisp configuration"
                            fakemake-feature)
                    ?\n ?\n)
            (prin1 `(add-to-list 'load-path ,(expand-file-name
                                              (ensure-string fakemake-feature)
                                              "/usr/share/emacs/site-lisp/"))
                   (current-buffer))
            (insert ?\n ?\n)
            (goto-char (point-max))
            (insert ?\n
                    (format ";;; begin: forms written by `%s'"
                            'autoload-generate-file-autoloads)
                    ?\n)
            (save-buffer)
            (let ((default-directory (expand-file-name ".." default-directory)))
              (do-default-directory-files (el-file (rx ".el" string-end) t t)
                ;; todo: deal with the case when el-file
                ;; specifies generated-autoload-file
                (insert ?\n)
                (let ((generated-autoload-load-name (file-name-base el-file)))
                  (autoload-generate-file-autoloads el-file (current-buffer)))))
            (insert ?\n
                    (format ";;; end: forms written by `%s'"
                            'autoload-generate-file-autoloads)
                    ?\n))
          (save-buffer) (kill-buffer)))))
  'autoloads)

(defmacro aif (test-form then-form &rest else-forms)
  (declare (indent 2))
  (let ((once-only (gensym "test-form-")))
    `(let ((,once-only ,test-form))
       (if ,once-only (let ((it ,once-only))
                        ;; (declare (type (not null) it))
                        ,then-form)
         (cl-locally ,@else-forms)))))

(defmacro awhen (test-form &rest then-forms)
  (declare (indent 1))
  `(let ((it ,test-form))
     (when it
       (cl-locally
        ;; (declare (type (not null) it))
        ,@then-forms))))

(defmacro aand (&rest conditions)
  "Like `and', but the result of the previous condition is bound to `it'.

The variable `it' is available within all CONDITIONS after the
initial one.

CONDITIONS are otherwise as documented for `and'.

Note that some implementations of this macro bind only the first
condition to `it', rather than each successive condition."
  (declare (debug t))
  (cond
    ((null conditions)
     t)
    ((null (cdr conditions))
     (car conditions))
    (t
     `(awhen ,(car conditions)
        (aand ,@(cdr conditions))))))

(defmacro aand1 (x &rest xs)
    "Like AAND but returns the value of X for true.

The first argument X is thus mandatory."
    (if xs
        `(let ((it ,x)) (when (aand it ,@xs) it))
      x))

(defsubst cl-plusp+
  (number)
  "When (CL-PLUSP NUMBER) is non-nil, return NUMBER."
  (when
      (cl-plusp number)
    number))

(defmacro with-el-files-from-elisp-source-directories-included-into ( var
                                                                      &rest
                                                                      body)
  (declare (indent 1))
  (cl-check-type var symbol)
  (if (eq 'el-files-from-elisp-source-directories var)
      ;; User shouldn't use this variable;
      ;; we probably better warn but I'm not in the mood right now.
      `(cl-locally ,@body)
    `(let ((,var
            (nconc
             (let ((el-files-from-elisp-source-directories
                    (mapcan (lambda (dir)
                              (let ((names
                                     (directory-files dir nil
                                                      (rx ".el" string-end))))
                                (let ((tail names))
                                  (while tail
                                    (setf (car tail)
                                          (file-relative-name
                                           (expand-file-name (car tail) dir)
                                           fakemake-project-root))
                                    (pop tail)))
                                names))
                            (setq fakemake-elisp-source-directories
                                  (sort fakemake-elisp-source-directories
                                        (lambda (x y)
                                          (aif (aand1 (cl-plusp+
                                                       (cl-mismatch x y :test #'char-equal))
                                                      (char-equal ?/ (aref x (1- it))))
                                              (cond
                                               ((= it (length x)) nil)
                                               ((= it (length y)) t)
                                               (t
                                                (string-lessp x y)
                                                ;; (string-lessp (substring x it)
                                                ;;               (substring y it))
                                                ))
                                            (string-lessp x y))))))))
               (when el-files-from-elisp-source-directories
                 (cl-flet ((files-equal (x y)
                             (string-equal
                              (file-relative-name x fakemake-project-root)
                              (file-relative-name y fakemake-project-root))))
                   (while (and
                           el-files-from-elisp-source-directories
                           (cl-member (car
                                       el-files-from-elisp-source-directories)
                                      ,var
                                      :test #'files-equal))
                     (warn "Duplicated file scheduled for compilation: %s"
                           ;; This should probably be style-warning
                           ;; but Elisp doesn't have those anyway.
                           (pop el-files-from-elisp-source-directories)))
                   (let ((tail el-files-from-elisp-source-directories))
                     (while (cdr tail)
                       (if (cl-member (cadr tail)
                                      ,var
                                      :test #'files-equal)
                           (warn "Duplicated file scheduled for compilation: %s"
                                 (pop (cdr tail)))
                         (pop tail))))))
               el-files-from-elisp-source-directories)
             ,var)))
       ,@body)))

(defvar fakemake-compile-error nil)

(defun fakemake-compile ()
  (fakemake-process-special-dirs)
  (with-el-files-from-elisp-source-directories-included-into
      fakemake-elisp-source-files-in-order
    "All files in elisp-source directories should be compiled"
    (do-files-list ((el-file "elisp file") fakemake-elisp-source-files-in-order)
      "Compiled %s file%s"
      ;; el-file should be relative to project root
      ;; in elisp-source-files-in-order already
      ;; for now we obsessively normalize
      ;; but this is wrong
      (let (exception)
        (cond
         ((string-prefix-p "fakemake/"
                           (file-relative-name el-file fakemake-project-root))
          ;; TODO: this should happen silently, we only leave it for debug
          (do-not-count "as element of %s" "fakemake/"))
         ;; there better be the following option:
         ;; if the file was tangled but was not decorated,
         ;; do not compile it either
         ((setq exception
                (cl-member (file-relative-name el-file fakemake-project-root)
                           fakemake-compile-regexps-blacklist
                           :test #'string-matched-p))
          (do-not-count "as blacklisted by %s" (car exception)))
         ((string-equal autoloads-file (file-name-base el-file))
          (do-not-count))
         ((byte-compile-file el-file)
          (cl-pushnew
           (file-relative-name (byte-compile-dest-file el-file)
                               ;; It would be much better if we could get
                               ;; the actual name of the compiled file
                               ;; directly.
                               fakemake-project-root)
           fakemake-files-to-install-into-lispdir
           :test #'string-equal)
          (cl-pushnew
           (file-relative-name el-file fakemake-project-root)
           fakemake-files-to-install-into-lispdir
           :test #'string-equal)
          ;; TODO: if the file in question is in
          ;; source-files-for-testing record its outputs as
          ;; code-files-for-testing
          nil)
         (t
          (do-not-count)
          (setf fakemake-compile-error t))))))

  (fakemake-config)
  (unless fakemake-compile-error
    'compiled))

(defun fakemake-test ()
  (fakemake-message "Trying to test using %s's own testing facility"
                    fakemake-feature)
  (if (null fakemake-org-files-for-testing-in-order)
      (error "%s provides no testing code we know of" fakemake-feature)
    (mapc (lambda (x) (load x nil t))
          fakemake-files-to-load-for-testing-in-order)
    ;; TODO: if the above run some ert functions, do not guess anything else
    (let ((run-tests-symbol (intern (format "%s-tests-run"
                                            fakemake-feature))))
      (if (fboundp run-tests-symbol)
          (funcall (symbol-function run-tests-symbol))
        (error "Don't know how to test"))))
  'tested)

(defun fakemake (&optional target live)
  "Kill Emacs in the end (with appropriate error code) when LIVE is nil."
  (setq target (or target 'default))
  (let ((fakemake-compile-error fakemake-compile-error))
    (let ((status (cl-ecase target
                    (default
                      (fakemake 'compile live))
                    (all
                     (fakemake 'default t))
                    (clean
                     (fakemake-message "making target %s" target)
                     (fakemake-clean))
                    (prepare
                     (if-let ((prepared (fakemake-donep 'prepare)))
                         (progn (fakemake-message "%s done:" target)
                                (load prepared nil t)
                                'prepared)
                       (fakemake-message "making target %s" target)
                       (when fakemake-use-lice-p (require 'lice))
                       (when-let ((status (fakemake-prepare)))
                         (fakemake-done-cache-variables 'prepare)
                         status)))
                    (compile
                     (if-let ((compiled (fakemake-donep 'compile)))
                         (progn (fakemake-message "%s done:" target)
                                (load compiled nil t)
                                'compiled)
                       (fakemake-message "making target %s" target)
                       (fakemake 'prepare t)
                       (when-let ((status (fakemake-compile)))
                         (fakemake-done-cache-variables 'compile)
                         status)))
                    (test
                     (fakemake-check-done compile
                       "Source had not been compiled.  Testing prior to compilation is not supported.")
                     (fakemake-test)))))
      (when (memq target '(clean prepare compile test))
        (fakemake-message "made target %s" target))
      (if live
          (unless fakemake-compile-error
            status)
        ;; we return 1 for compile-error
        ;; because that's what batch-byte-compile-file returns
        (kill-emacs (if fakemake-compile-error 1 0))))))
